package name.khartn.KLADRtoMySQL;

import com.hexiong.jdbf.DBFReader;
import com.hexiong.jdbf.JDBFException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Husnutdinov
 */
public class KladrThread extends Thread {

    @Override
    public void run() {
        try {

            Connection connection;
            String driverName = "com.mysql.jdbc.Driver";

            Class.forName(driverName);

            // Create a connection to the database
            String serverName = "localhost";
            String mydatabase = "kladr";
            String url = "jdbc:mysql://" + serverName + "/" + mydatabase;
            String username = "root";
            String password = ";

            connection = DriverManager.getConnection(url, username, password);
            System.out.println("is connect to DB");

            //            String query = "Select * FROM news";
            //            Statement stmt = connection.createStatement();
            //
            //            ResultSet rs = stmt.executeQuery(query);
            //            String dbtime;
            //            while (rs.next()) {
            //                dbtime = rs.getString(1);
            //                System.out.println(dbtime);
            //            } // end while



            Statement stmt = connection.createStatement();


            stmt.executeUpdate("drop table if exists kladr;");

            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS `kladr` (\n"
                    + "  `NAME` varchar(50) NOT NULL,\n"
                    + "  `SOCR` varchar(20) NOT NULL,\n"
                    + "  `CODE` varchar(20) NOT NULL,\n"
                    + "  `INDEX` varchar(10) NOT NULL,\n"
                    + "  `GNINMB` varchar(10) NOT NULL,\n"
                    + "  `UNO` varchar(10) NOT NULL,\n"
                    + "  `OCATD` varchar(20) NOT NULL,\n"
                    + "  `STATUS` varchar(5) NOT NULL,\n"
                    + "  `kod_subj` varchar(777) NOT NULL DEFAULT '',\n"
                    + "  `kod_rayona` varchar(777) NOT NULL DEFAULT '',\n"
                    + "  `kod_goroda` varchar(777) NOT NULL DEFAULT '',\n"
                    + "  `kod_nasel_punkt` varchar(777) NOT NULL DEFAULT '',\n"
                    + "  `priznak_akt` varchar(777) NOT NULL DEFAULT ''\n"
                    + ") ENGINE=MyISAM DEFAULT CHARSET=utf8;");

            DBFReader dbfreader = new DBFReader("c:\\temp\\BASE\\KLADR.DBF");
            int i;
            for (i = 0; i < dbfreader.getFieldCount(); i++) {
                System.out.print(dbfreader.getField(i).getName() + "  ");
            }
            System.out.print("\n");
            for (i = 0; dbfreader.hasNextRecord(); i++) {
                Object aobj[] = dbfreader.nextRecord(Charset.forName("IBM866"));
                String query = "insert into kladr values(";
                for (int j = 0; j < aobj.length; j++) {
                    //                    System.out.print(aobj[j] + "  |  ");
                    query += " '" + aobj[j] + "', ";
                }
                query = query.substring(0, query.length() - 2);
                query = query + ",'','','','','');";
                stmt.executeUpdate(query);
            }

            stmt.executeUpdate("update kladr \n"
                    + "set kod_subj=substr(CODE, 1,2), \n"
                    + "kod_rayona=substr(CODE, 3,3),\n"
                    + "kod_goroda=substr(CODE, 6,3),\n"
                    + "kod_nasel_punkt=substr(CODE, 9,3),\n"
                    + "priznak_akt=substr(CODE, 12,2)");

            System.out.println("Total Count: " + i);

            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(KladrThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(KladrThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JDBFException ex) {
            Logger.getLogger(KladrThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
