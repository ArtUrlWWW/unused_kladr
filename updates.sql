ALTER TABLE  `kladr_1` ADD  `kod_subj` VARCHAR( 777 ) NOT NULL DEFAULT  '',
ADD  `kod_rayona` VARCHAR( 777 ) NOT NULL DEFAULT  '',
ADD  `kod_goroda` VARCHAR( 777 ) NOT NULL DEFAULT  '',
ADD  `kod_nasel_punkt` VARCHAR( 777 ) NOT NULL DEFAULT  '',
ADD  `priznak_akt` VARCHAR( 777 ) NOT NULL DEFAULT  ''

update kladr_1 
set kod_subj=substr(CODE, 1,2), 
kod_rayona=substr(CODE, 3,3),
kod_goroda=substr(CODE, 6,3),
kod_nasel_punkt=substr(CODE, 9,3),
priznak_akt=substr(CODE, 12,2)

update street_1 
set kod_subj=substr(CODE, 1,2), 
kod_rayona=substr(CODE, 3,3),
kod_goroda=substr(CODE, 6,3),
kod_nasel_punkt=substr(CODE, 9,3),
kod_ulici=substr(CODE, 12,4),
priznak_akt=substr(CODE, 16,2)

update doma_1 
set kod_subj=substr(CODE, 1,2), 
kod_rayona=substr(CODE, 3,3),
kod_goroda=substr(CODE, 6,3),
kod_nasel_punkt=substr(CODE, 9,3),
kod_ulici=substr(CODE, 12,4),
potyadkoviy_nomer=substr(CODE, 16,4)