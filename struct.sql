-- phpMyAdmin SQL Dump
-- version 4.0.0
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: May 17, 2013 at 11:32 AM
-- Server version: 5.6.10
-- PHP Version: 5.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `kladr`
--

-- --------------------------------------------------------

--
-- Table structure for table `altnames`
--

CREATE TABLE IF NOT EXISTS `altnames` (
  `OLDCODE` varchar(777) NOT NULL,
  `NEWCODE` varchar(777) NOT NULL,
  `LEVEL` varchar(777) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doma`
--

CREATE TABLE IF NOT EXISTS `doma` (
  `NAME` varchar(777) NOT NULL,
  `KORP` varchar(777) NOT NULL,
  `SOCR` varchar(777) NOT NULL,
  `CODE` varchar(777) NOT NULL,
  `INDEX` varchar(777) NOT NULL,
  `GNINMB` varchar(777) NOT NULL,
  `UNO` varchar(777) NOT NULL,
  `OCATD` varchar(777) NOT NULL,
  `kod_subj` varchar(777) NOT NULL DEFAULT '',
  `kod_rayona` varchar(777) NOT NULL DEFAULT '',
  `kod_goroda` varchar(777) NOT NULL DEFAULT '',
  `kod_nasel_punkt` varchar(777) NOT NULL DEFAULT '',
  `kod_ulici` varchar(777) NOT NULL DEFAULT '',
  `potyadkoviy_nomer` varchar(777) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kladr`
--

CREATE TABLE IF NOT EXISTS `kladr` (
  `NAME` varchar(50) NOT NULL,
  `SOCR` varchar(20) NOT NULL,
  `CODE` varchar(20) NOT NULL,
  `INDEX` varchar(10) NOT NULL,
  `GNINMB` varchar(10) NOT NULL,
  `UNO` varchar(10) NOT NULL,
  `OCATD` varchar(20) NOT NULL,
  `STATUS` varchar(5) NOT NULL,
  `kod_subj` varchar(777) NOT NULL DEFAULT '',
  `kod_rayona` varchar(777) NOT NULL DEFAULT '',
  `kod_goroda` varchar(777) NOT NULL DEFAULT '',
  `kod_nasel_punkt` varchar(777) NOT NULL DEFAULT '',
  `priznak_akt` varchar(777) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `kod_subj` (`kod_subj`(333)),
  KEY `kod_rayona` (`kod_rayona`(333)),
  KEY `kod_goroda` (`kod_goroda`(333)),
  KEY `kod_nasel_punkt` (`kod_nasel_punkt`(333)),
  KEY `priznak_akt` (`priznak_akt`(333)),
  KEY `NAME` (`NAME`),
  KEY `SOCR` (`SOCR`),
  KEY `CODE` (`CODE`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;

-- --------------------------------------------------------

--
-- Table structure for table `socrbase`
--

CREATE TABLE IF NOT EXISTS `socrbase` (
  `LEVEL` varchar(777) NOT NULL,
  `SCNAME` varchar(777) NOT NULL,
  `SOCRNAME` varchar(777) NOT NULL,
  `KOD_T_ST` varchar(777) NOT NULL,
  KEY `SCNAME` (`SCNAME`(333)),
  KEY `LEVEL` (`LEVEL`(333)),
  KEY `SOCRNAME` (`SOCRNAME`(333))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `street`
--

CREATE TABLE IF NOT EXISTS `street` (
  `NAME` varchar(50) NOT NULL,
  `SOCR` varchar(20) NOT NULL,
  `CODE` varchar(20) NOT NULL,
  `INDEX` varchar(10) NOT NULL,
  `GNINMB` varchar(10) NOT NULL,
  `UNO` varchar(10) NOT NULL,
  `OCATD` varchar(20) NOT NULL,
  `kod_subj` varchar(777) NOT NULL DEFAULT '',
  `kod_rayona` varchar(777) NOT NULL DEFAULT '',
  `kod_goroda` varchar(777) NOT NULL DEFAULT '',
  `kod_nasel_punkt` varchar(777) NOT NULL DEFAULT '',
  `kod_ulici` varchar(777) NOT NULL DEFAULT '',
  `priznak_akt` varchar(777) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `kod_subj` (`kod_subj`(333)),
  KEY `kod_rayona` (`kod_rayona`(333)),
  KEY `kod_goroda` (`kod_goroda`(333)),
  KEY `kod_ulici` (`kod_ulici`(333)),
  KEY `kod_nasel_punkt` (`kod_nasel_punkt`(333)),
  KEY `priznak_akt` (`priznak_akt`(333)),
  KEY `NAME` (`NAME`),
  KEY `SOCR` (`SOCR`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;
