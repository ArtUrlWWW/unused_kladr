/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package name.khartn.KLADRtoMySQL;

import com.hexiong.jdbf.DBFReader;
import com.hexiong.jdbf.JDBFException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Husnutdinov
 */
public class SOCRBASEThread extends Thread {

    @Override
    public void run() {
        try {

            Connection connection;
            String driverName = "com.mysql.jdbc.Driver";

            Class.forName(driverName);

            // Create a connection to the database
            String serverName = "localhost";
            String mydatabase = "kladr";
            String url = "jdbc:mysql://" + serverName + "/" + mydatabase;
            String username = "root";
            String password = ";

            connection = DriverManager.getConnection(url, username, password);
            System.out.println("is connect to DB");

            //            String query = "Select * FROM news";
            //            Statement stmt = connection.createStatement();
            //
            //            ResultSet rs = stmt.executeQuery(query);
            //            String dbtime;
            //            while (rs.next()) {
            //                dbtime = rs.getString(1);
            //                System.out.println(dbtime);
            //            } // end while



            Statement stmt = connection.createStatement();



            stmt.executeUpdate("drop table if exists SOCRBASE;");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS `socrbase` (\n"
                    + "  `LEVEL` varchar(777) NOT NULL,\n"
                    + "  `SCNAME` varchar(777) NOT NULL,\n"
                    + "  `SOCRNAME` varchar(777) NOT NULL,\n"
                    + "  `KOD_T_ST` varchar(777) NOT NULL\n"
                    + ") ENGINE=MyISAM DEFAULT CHARSET=utf8;");

            int i = 0;
            DBFReader dbfreader = new DBFReader("c:\\temp\\BASE\\SOCRBASE.DBF");
            for (i = 0; i < dbfreader.getFieldCount(); i++) {
                System.out.print(dbfreader.getField(i).getName() + "  ");
            }
            System.out.print("\n");
            for (i = 0; dbfreader.hasNextRecord(); i++) {
                Object aobj[] = dbfreader.nextRecord(Charset.forName("IBM866"));
                String query = "insert into socrbase values(";
                for (int j = 0; j < aobj.length; j++) {
//                    System.out.print(aobj[j] + "  |  ");
                    query += " '" + aobj[j] + "', ";
                }
                query = query.substring(0, query.length() - 2);
                query = query + ");";
                stmt.executeUpdate(query);
            }

            System.out.println("Total Count: " + i);

            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(SOCRBASEThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SOCRBASEThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JDBFException ex) {
            Logger.getLogger(SOCRBASEThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
