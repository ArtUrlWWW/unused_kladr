package name.khartn.KLADRtoMySQL;

import com.hexiong.jdbf.DBFReader;
import com.hexiong.jdbf.JDBFException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Husnutdinov
 */
public class StreetThread extends Thread {

    @Override
    public void run() {
        try {

            Connection connection;
            String driverName = "com.mysql.jdbc.Driver";

            Class.forName(driverName);

            // Create a connection to the database
            String serverName = "localhost";
            String mydatabase = "kladr";
            String url = "jdbc:mysql://" + serverName + "/" + mydatabase;
            String username = "root";
            String password = ";

            connection = DriverManager.getConnection(url, username, password);
            System.out.println("is connect to DB");

            Statement stmt = connection.createStatement();

            stmt.executeUpdate("drop table if exists street;");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS `street` (\n"
                    + "  `NAME` varchar(50) NOT NULL,\n"
                    + "  `SOCR` varchar(20) NOT NULL,\n"
                    + "  `CODE` varchar(20) NOT NULL,\n"
                    + "  `INDEX` varchar(10) NOT NULL,\n"
                    + "  `GNINMB` varchar(10) NOT NULL,\n"
                    + "  `UNO` varchar(10) NOT NULL,\n"
                    + "  `OCATD` varchar(20) NOT NULL,\n"
                    + "  `kod_subj` varchar(777) NOT NULL DEFAULT '',\n"
                    + "  `kod_rayona` varchar(777) NOT NULL DEFAULT '',\n"
                    + "  `kod_goroda` varchar(777) NOT NULL DEFAULT '',\n"
                    + "  `kod_nasel_punkt` varchar(777) NOT NULL DEFAULT '',\n"
                    + "  `kod_ulici` varchar(777) NOT NULL DEFAULT '',\n"
                    + "  `priznak_akt` varchar(777) NOT NULL DEFAULT ''\n"
                    + ") ENGINE=MyISAM DEFAULT CHARSET=utf8;");

            int i = 0;
            DBFReader dbfreader = new DBFReader("c:\\temp\\BASE\\STREET.DBF");
            for (i = 0; i < dbfreader.getFieldCount(); i++) {
                System.out.print(dbfreader.getField(i).getName() + "  ");
            }
            System.out.print("\n");
            for (i = 0; dbfreader.hasNextRecord(); i++) {
                Object aobj[] = dbfreader.nextRecord(Charset.forName("IBM866"));
                String query = "insert into street values(";
                for (int j = 0; j < aobj.length; j++) {
//                    System.out.print(aobj[j] + "  |  ");
                    query += " '" + aobj[j] + "', ";
                }
                query = query.substring(0, query.length() - 2);
                query = query + ",'','','','','','');";
                stmt.executeUpdate(query);
            }

            stmt.executeUpdate("update street \n"
                    + "set kod_subj=substr(CODE, 1,2), \n"
                    + "kod_rayona=substr(CODE, 3,3),\n"
                    + "kod_goroda=substr(CODE, 6,3),\n"
                    + "kod_nasel_punkt=substr(CODE, 9,3),\n"
                    + "kod_ulici=substr(CODE, 12,4),\n"
                    + "priznak_akt=substr(CODE, 16,2)");

            System.out.println("Total Count: " + i);

            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(StreetThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StreetThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JDBFException ex) {
            Logger.getLogger(StreetThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
