package name.khartn.KLADRtoMySQL;

/**
 *
 * @author Husnutdinov
 */
public class JdbfTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        KladrThread kladrThread = new KladrThread();
        kladrThread.start();

        StreetThread streetThread = new StreetThread();
        streetThread.start();

        DomaThread domaThread = new DomaThread();
        domaThread.start();

        SOCRBASEThread sOCRBASEThread = new SOCRBASEThread();
        sOCRBASEThread.start();

        AltnamesThread altnamesThread = new AltnamesThread();
        altnamesThread.start();
    }
}
